import { DataObject, EdmMapping, EdmType, DataPermissionEventListener, DataObjectState } from '@themost/data';
import { EducationalCredentialsValidateResult } from './EducationalCredentialsValidateResult';
import { TraceUtils } from '@themost/common';
import { promisify } from 'es6-promisify';


@EdmMapping.entityType('EducationalOccupationalCredentials')
class EducationalOccupationalCredentials extends DataObject {
    
    id;
    competencyRequired;
    credentialCategory;
    educationalLevel;
    recognizedBy;
    
    constructor() {
        super();
    }

    /**
     * Returns a collection of rules which are going to be checked
     * @returns {Promise<any[]>}
     */
     @EdmMapping.func('Rules', EdmType.CollectionOf('Rule'))
     async getRules() {
        const Rule = this.context.model('Rule').getDataObjectType();
         return Rule.expand(this.context, this.getId().toString(),
             'EducationalOccupationalCredentials', 'EducationalOccupationalCredentialsRule');
     }

    /**
     * @returns {Promise<any[]>}
     */
     @EdmMapping.param('items', EdmType.CollectionOf('Rule'), false, true)
     @EdmMapping.action('Rules', EdmType.CollectionOf('Rule'))
     async setRules(items) {
         const finalItems = [];
         // get Rule class
         const Rule = this.context.model('Rule').getDataObjectType();
         for (let item of items) {
             // convert item
             const converted = await Rule.flatten(this.context, item, `${item.refersTo}RuleEx`);
             // set target id
             converted.target = this.getId().toString();
             // set target type
             converted.targetType = 'EducationalOccupationalCredentials';
             // set additional type
             converted.additionalType = 'EducationalOccupationalCredentialsRule';
             // add item
             finalItems.push(converted);
         }
         // validate update permission
         const validateAsync = promisify(DataPermissionEventListener.prototype.validate);
         await validateAsync({
             model: this.getModel(), // set current model
             state: DataObjectState.Update, // set state to update
             target: this // this object is the target object
         });
         // save items
         await this.context.model(Rule).silent().save(finalItems);
         // and return new collection
         return this.getRules();
     }

     async validate(student) {
        const context = this.context;
        const studentObject = await context.model('Student').where("id").equal(student).silent().getTypedItem();
        const data = {
            student: studentObject
        };
        // validate rules
        const rules = await context.model('Rule')
            .where('additionalType').equal('EducationalOccupationalCredentialsRule')
            .and('targetType').equal('EducationalOccupationalCredentials')
            .and('target').equal(this.getId().toString()).getItems();

        if (rules && rules.length === 0) {
            return;
        }
        let validationResults = [];
        // add async function for validating graduation rules
        let forEachRule = (educationCredentialsRule) => {
            try {
                return new Promise((resolve) => {
                    const ruleModel = context.model(`${educationCredentialsRule.refersTo}Rule`);
                    if (ruleModel == null) {
                        validationResults = validationResults || [];
                        const errorResult = {
                            success: false,
                            statusCode :  422,
                            code: 'FAIL',
                            message: 'Student validation rule type cannot be found.'
                        };
                        // new ValidationResult(false, 'FAIL', 'Student validation rule type cannot be found.');
                        validationResults.push(errorResult);
                        educationCredentialsRule.validationResult = errorResult;
                        return resolve();
                    }
                    const rule = ruleModel.convert(educationCredentialsRule);
                    rule.validate(data, function (err, result) {
                        if (err) {
                            validationResults = validationResults || [];
                            const errorResult = {
                                success: false,
                                statusCode :  422,
                                code: 'FAIL',
                                message: err.message
                            }; // new ValidationResult(false, 'FAIL', err.message);
                            validationResults.push(errorResult);
                            educationCredentialsRule.validationResult = errorResult;
                            return resolve();
                        }
                        /**
                         * @type {ValidationResult[]}
                         */
                        validationResults = validationResults || [];
                        validationResults.push(result);
                        educationCredentialsRule.validationResult = result;
                        return resolve();
                    });
                });
            } catch (err) {
                TraceUtils.error(err);
            }
        };
        for (let rule of rules) {
            await forEachRule(rule);
        }

        const fnValidateExpression = function (x) {
            try {
                let expr = x.ruleExpression;
                if (expr == null) {
                    return false;
                }
                if (expr.length === 0) {
                    return false;
                }
                expr = expr.replace(/\[%(\d+)]/g, function () {
                    if (arguments.length === 0) return;
                    const id = parseInt(arguments[1]);
                    const v = validationResults.find(function (y) {
                        return y.id === id;
                    });
                    if (v) {
                        return v.success.toString();
                    }
                    return 'false';
                });
                expr = expr.replace(/\bAND\b/ig, ' && ').replace(/\bOR\b/ig, ' || ').replace(/\bNOT\b/ig, ' !');
                return eval(expr);
            } catch (e) {
                return e;
            }
        };

        const fnTitleExpression = function (x) {
            try {
                let expr = x.ruleExpression;
                if (expr == null) {
                    return false;
                }
                if (expr.length === 0) {
                    return false;
                }
                expr = expr.replace(/\[%(\d+)]/g, function () {
                    if (arguments.length === 0) return;
                    const id = parseInt(arguments[1]);
                    const v = validationResults.find(function (y) {
                        return y.id === id;
                    });
                    if (v) {
                        return '(' + v.message.toString() + ')';
                    }
                    return 'Unknown Rule';
                });
                expr = expr.replace(/\bAND\b/ig, context.__(' AND ')).replace(/\bOR\b/ig, context.__(' OR ')).replace(/\bNOT\b/ig, context.__(' NOT '));
                return expr;
            } catch (e) {
                return e;
            }
        };

        let res, title;

        //apply default expression
        const expr = rules.find(function (x) {
            return x['ruleExpression'] != null;
        });
        let finalResult;
        if (expr) {
            res = fnValidateExpression(expr);
            title = fnTitleExpression(expr);
            finalResult = {
                success: res === true,
                statusCode :  res === true ? 200 : 422,
                code: res === true ? 'SUCC' : 'FAIL',
                message: title
            }; // new ValidationResult(res, res === true ? 'SUCC' : 'FAIL', title);
            finalResult.type = 'EducationalCredentialsRule';
            expr.ruleExpression = expr.ruleExpression.replace(/\bAND\b/ig, ' && ').replace(/\bOR\b/ig, ' || ').replace(/\bNOT\b/ig, ' !');
            expr.ruleExpression = expr.ruleExpression.replace(/\[%(\d+)]/g, function () {
                if (arguments.length === 0) return;
                return parseInt(arguments[1]);
            });

        } else {
            //get expression (for this rule type)
            const ruleExp1 = rules.map(function (x) {
                return '[%' + x.id + ']';
            }).join(' AND ');
            res = fnValidateExpression({ruleExpression: ruleExp1});
            title = fnTitleExpression({ruleExpression: ruleExp1});
            finalResult = {
                success: res === true,
                statusCode :  res === true ? 200 : 422,
                code: res === true ? 'SUCC' : 'FAIL',
                message: title
            }; // new ValidationResult(res, res === true ? 'SUCC' : 'FAIL', title);
            finalResult.type = 'EducationalOccupationalCredentialsRule';
            finalResult.expression = null;
        }
        // add data result to finalResult from rules MeanGrade, YearMeanGrade
        const resultData = rules.find(x => {
            return x.refersTo === 'MeanGrade' || x.refersTo === 'YearMeanGrade';
        });
        finalResult.data = resultData && resultData.validationResult ? resultData.validationResult.data : {};
        return {finalResult, "rules": rules};
    }

 
     /**
      * Checks a collection of students against the rules defined for this item
      * @returns {Promise<any[]>}
      */
     @EdmMapping.param('items', EdmType.CollectionOf('Student'), false, true)
     @EdmMapping.action('checkStudents', 'EducationalCredentialsValidateAction')
     async checkStudents(items) {
         try {
             // check all students
             const action = await this.context.model('EducationalCredentialsValidateAction').save(
                 {
                    object: this.id,
                    totalStudents: items.length
                 });
             await EducationalOccupationalCredentials.checkRules(this.context, this.id, items, action);
             return action;
         } catch (err) {
             throw (err);
         }
     }

     static checkRules(context, object, students, action) {
        const app = context.getApplication();
        const thisContext = app.createContext();
        Object.assign(thisContext, {
            user: context.user
        });
        (async function () {
            // check rules for each student
            // validate rules
            object = thisContext.model(EducationalOccupationalCredentials).convert(object);
            for (let i = 0; i < students.length; i++) {
                let student = students[i];
                try {
                    await new Promise((resolve, reject) => {
                        return object.validate(student.id).then(validationResult => {
                            const result = {
                                action: action.id,
                                student: student.id
                            };
                            if (validationResult && validationResult.finalResult) {
                                result.result = validationResult.finalResult.success;
                                result.grade = validationResult.finalResult.data && validationResult.finalResult.data.finalGrade;

                            } else {
                                result.result = false;
                                TraceUtils.error('Could not validate student educational credentials.');
                            }
                            return thisContext.model(EducationalCredentialsValidateResult).silent().save(result).then(res => {
                                return resolve();
                            })
                        }).catch(err => {
                            return resolve();
                        });


                    });

                } catch (err) {
                    TraceUtils.error(err);
                }
            }
        })().then(() => {
            thisContext.finalize(() => {
                // after finishing sending mails, an instructor message is created to inform instructor
                action.actionStatus = {alternateName: 'CompletedActionStatus'};
                action.endTime = new Date();
                return thisContext.model('EducationalCredentialsValidateAction').silent().save(action).then(action => {
                }).catch(err => {
                    TraceUtils.error(err);
                });
            });
        }).catch(err => {
            thisContext.finalize(() => {
                action.actionStatus = {alternateName: 'FailedActionStatus'};
                action.endTime = new Date();
                action.description = err.message;
                return thisContext.model('EducationalCredentialsValidateAction').silent().save(action).then(action => {
                }).catch(err => {
                    TraceUtils.error(`An error occurred while checking rules for educational credentials with id ${object}`);
                    TraceUtils.error(err);
                });
            });
        });
    }
}

export {
    EducationalOccupationalCredentials
}